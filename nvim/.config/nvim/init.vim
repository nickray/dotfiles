" Specify a directory for plugins
call plug#begin('~/.local/share/nvim/plugged')

" For `:help plug-options
Plug 'junegunn/vim-plug'

" For consistency
Plug 'editorconfig/editorconfig-vim'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

Plug 'rust-lang/rust.vim'

Plug 'ambv/black'
autocmd BufWritePre *.py execute ':Black'
let g:black_skip_string_normalization = 1

Plug 'junegunn/goyo.vim'

Plug 'itchyny/lightline.vim'
" not necessary anymore
set noshowmode
" let g:lightline = {
"       \ 'colorscheme': 'solarized light',
"       \ }

" use `gc` to toggle commentary
Plug 'tpope/vim-commentary'

Plug 'autozimu/LanguageClient-neovim', { 
    \ 'branch': 'next', 
    \ 'do': 'bash install.sh',
    \ }

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
let g:deoplete#enable_at_startup = 1

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Initialize plugin system
call plug#end()

" Modifications
set number
nnoremap <silent><F2> :set nonumber!<CR>:set foldcolumn=0<CR>

set pastetoggle=<F3>

nnoremap <silent> <F4> :NERDTreeToggle<CR>

" switch higlight no matter the previous state
nnoremap <silent> <F6> :set hls! <cr>
" hit '/' highlights then enter search mode
nnoremap / :set hlsearch<cr>/

" strange, needed at least on Debian 9 middle-click paste
if has('mouse')
    set mouse=r
endif

" everything from now on for rustlang rls

" Required for operations modifying multiple buffers like rename.
set hidden

let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'nightly', 'rls'],
    \ }

nnoremap <F5> :call LanguageClient_contextMenu()<CR>

