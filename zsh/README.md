The zsh configuration is based on [Grml](https://grml.org/zsh/#grmlzshconfig)  

On Arch Linux:
  `pacman -S grml-zsh-config`

On other distributions: 
  `wget -O ~/.zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc`

Local modifications belong in an untracked file ~/zshrc.local
