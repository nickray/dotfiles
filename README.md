README
======

To install any dotfiles,

* install stow
* stow `program`

Subdirectories may have additional README.md files.

One reference for this idea: https://git.io/.files

[1] On Debian 8, apply this patch to stow:
http://lists.gnu.org/archive/html/bug-stow/2014-06/msg00000.html
