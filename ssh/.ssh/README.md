Local modifications belong in an untracked file ~/.ssh/config.local 

To ensure .gitignore works for SSH keys, generate with ending .key  

The ssh config assumes a naming scheme of `user`-ed@`machine`.key,
a 256 bit ed25519 key. For old sshd installations, generate 2048 bit
RSA keys named `user`-rsa@`machine`.key
